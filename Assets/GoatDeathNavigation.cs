﻿using System.Collections;
using System.Collections.Generic;
using Assets.Lib;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoatDeathNavigation : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene(Constants.SceneNameGame, LoadSceneMode.Single);
    }

    public void GoBackToMainMenu()
    {
        SceneManager.LoadScene(Constants.SceneNameMainMenu, LoadSceneMode.Single);
    }
}
