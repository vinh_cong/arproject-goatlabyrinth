﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuGoatRotationController : MonoBehaviour
{
    public float RotationSpeed = 5;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Rotate(0, Time.deltaTime * RotationSpeed, 0);
    }
}
