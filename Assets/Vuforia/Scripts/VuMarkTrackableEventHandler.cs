/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using UnityEngine;

namespace Vuforia
{
    /// <summary>
    /// A custom handler that implements the ITrackableEventHandler interface.
    /// </summary>
    public class VuMarkTrackableEventHandler : MonoBehaviour,
                                                ITrackableEventHandler
    {
        public GameObject[] Models = new GameObject[0];

        #region PRIVATE_MEMBER_VARIABLES

        private TrackableBehaviour mTrackableBehaviour;

        #endregion // PRIVATE_MEMBER_VARIABLES



        #region UNTIY_MONOBEHAVIOUR_METHODS

        void Start()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
        }

        #endregion // UNTIY_MONOBEHAVIOUR_METHODS



        #region PUBLIC_METHODS

        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        #endregion // PUBLIC_METHODS



        #region PRIVATE_METHODS


        private void OnTrackingFound()
        {
            Transform[] objects = GetComponentsInChildren<Transform>(true);

            // Check if a given immidiate child match the id of the vumark

            // Enable rendering:
            foreach (GameObject component in Models)
            {
                if (component == null)
                    continue;

                // Check if the vumark ID matches the current visible vumark
                VuMarkBehaviour behaviour = this.GetComponent<VuMarkBehaviour>();
                VuMarkTarget target = behaviour.VuMarkTarget;
                VuMarkID assignedID = component.GetComponent<VuMarkID>();

                if (target == null || assignedID == null)
                    continue;

                string vuMarkID = target.InstanceId.StringValue;
                string assignedVuMarkID = assignedID.id;

                // Non matching id, skip.
                if (!vuMarkID.Equals(assignedVuMarkID))
                    continue;

                // All good, render it.
                component.GetComponent<Renderer>().enabled = true;
                component.GetComponent<Collider>().enabled = true;
                RenderComponentChildren(component);
            }

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
        }

        private void RenderComponentChildren(GameObject gameobject)
        {
            Renderer[] rendererComponents = gameobject.GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = gameobject.GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = true;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = true;
            }
        }

        private void OnTrackingLost()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = false;
            }

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
        }

        #endregion // PRIVATE_METHODS
    }
}
