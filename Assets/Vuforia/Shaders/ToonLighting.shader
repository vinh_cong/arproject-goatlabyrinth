﻿Shader "AR/Toon" {
	Properties {
		_MainTex ("MainTex", 2D) = "white" {}
		_Ambient("Ambient", Color) = (0.1,0.1,0.1,1)
		_DiffuseFactor("Color", Color) = (1,1,1,1)
		_Threshold("Threshold", Range(-1,1)) = 0.3
		_Contour("Contour", Range(0,1)) = 0.1
		_ContourBrightness("ContourBrightness", Range(0,1)) = 0.3
		_Sharpness("Sharpness", Range(0,20)) = 10
	}
	SubShader {
		//Make sure the light data of the scene gets updated for this shader
		Tags{ "LightMode" = "ForwardBase" }
		
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			fixed4 _Ambient;
			fixed4 _Specular;
			fixed4 _DiffuseFactor;
			float _Threshold;
			float _Sharpness;
			float _Contour;
			float _ContourBrightness = 0.3;

			struct Input
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				half3 normal : NORMAL;
			};

			struct VertexToFragment
			{
				float4 vertex : SV_POSITION;
				float4 camPos : TEXCOORD1;
				float2 uv : TEXCOORD0;
				half3 normal : NORMAL;
			};

			VertexToFragment vert(Input IN) {
				VertexToFragment OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.camPos = mul(UNITY_MATRIX_M, IN.vertex);
				OUT.uv = IN.uv;
				OUT.normal = UnityObjectToWorldNormal(IN.normal);
				return OUT;
			}

			fixed4 frag(VertexToFragment IN) : SV_Target {

				float3 vertexToCamera = normalize(_WorldSpaceCameraPos-IN.camPos);

				float viewDot = dot(IN.normal,vertexToCamera);

				viewDot = clamp((viewDot-_Contour)*_Sharpness, _ContourBrightness, 1);

				float brightness = max(0,dot(IN.normal,-_WorldSpaceLightPos0.xyz));
				brightness = clamp((brightness-_Threshold) * _Sharpness,_ContourBrightness,1) * viewDot;

				fixed4 lightDiffuse = _DiffuseFactor * brightness + _Ambient;
				fixed4 texAlbedo = tex2D(_MainTex, IN.uv);
				return texAlbedo * lightDiffuse;
			}

			ENDCG
		}
		
	}
}
