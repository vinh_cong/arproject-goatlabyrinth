﻿//Shader for single light source (just picks the first light source in the scene)
//Light calculations on a per-vertex base

Shader "AR/VertexLighting" {

	Properties {
		_Ambient ("Ambient", Color) = (1,1,1,1)
		_Diffuse("Diffuse", Color) = (0.9,0.9,0.9,1)
		_Specular ("Specular", Color) = (0.75,0.75,0.75,1)
		_SpecularExponent ("Shininess", Range(1,64)) = 40
	}

	SubShader {

		Pass {
			//Make sure the light data of the scene gets updated for this shader
			Tags{ "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#include "Lighting.cginc" //To access lights in the unity scene
			//See also https://docs.unity3d.com/462/Documentation/Manual/SL-BuiltinValues.html

			//Declare the properties within CG program
			fixed4 _Ambient;
			fixed4 _Specular;
			fixed4 _Diffuse;
			float _SpecularExponent;

			struct Input
			{
				float4 vertex : POSITION;
				float4 diffuse : COLOR;
				float3 normal : NORMAL;
			};

			struct VertexToFragment
			{
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
			};

			VertexToFragment vert(Input IN) {
				VertexToFragment OUT;

				//Calculate necessary points/vectors
				float3 worldPos = mul(UNITY_MATRIX_M, IN.vertex);
				float3 lightDirection = _WorldSpaceLightPos0.xyz;
				half3 worldNormal = UnityObjectToWorldNormal(IN.normal);
				
				//Calculate ambient color
				float3 Iamb = _Ambient * unity_AmbientSky;
				//Calculate diffuse color
				float3 Idiff = _Diffuse * IN.diffuse * _LightColor0 * max(0, dot(worldNormal, lightDirection));
				//Calculate specular color
				float3 viewVector = normalize(_WorldSpaceCameraPos - worldPos);
				float3 halfVector = normalize(viewVector + lightDirection);
				float3 Ispec = _Specular * _LightColor0 * pow(max(0,dot(halfVector, worldNormal)), _SpecularExponent);
				
				//Output values
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.color = float4(Iamb + Idiff + Ispec,1);
				return OUT;
			}

			fixed4 frag(VertexToFragment IN) : SV_Target {
				//We just return the (interpolated) vertex color
				return IN.color;
			}

			ENDCG
		}
	}
}
