﻿//Shader for single light source (just picks the first light source in the scene)
//Light calculations on a per-fragment (Phong shading)

Shader "AR/FragmentLighting" {

	Properties {
		_AlbedoTex("AlbedoTexture", 2D) = "white" {}
		_Ambient ("Ambient", Color) = (1,1,1,1)
		_Diffuse("Diffuse", Color) = (0.9,0.9,0.9,1)
		_Specular("Specular", Color) = (0.75,0.75,0.75,1)
		_SpecularExponent ("Shininess", Range(1,64)) = 40
	}

	SubShader {

		Pass {
			//Make sure the light data of the scene gets updated for this shader
			Tags{ "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#include "Lighting.cginc" //To access lights in the unity scene
			//See also https://docs.unity3d.com/462/Documentation/Manual/SL-BuiltinValues.html

			//Declare the properties within CG program
			fixed4 _Ambient;
			fixed4 _Specular;
			fixed4 _Diffuse;
			float _SpecularExponent;
			sampler2D _AlbedoTex;

			struct Input
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct VertexToFragment
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 worldPos : TEXCOORD1;
				float3 normal : NORMAL;
			};

			VertexToFragment vert(Input IN) {
				VertexToFragment OUT;

				OUT.worldPos = mul(UNITY_MATRIX_M, IN.vertex);
				OUT.normal = UnityObjectToWorldNormal(IN.normal);
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.uv = IN.uv;

				return OUT;
			}

			fixed4 frag(VertexToFragment IN) : SV_Target {
				float3 lightDirection = _WorldSpaceLightPos0.xyz;

				//An interpolated vector typically does not have unit length. Needs to be normalized after interpolation
				float3 normal = normalize(IN.normal);

				//The reflectance of the material differs along the surface (texture)
				fixed4 albedo = tex2D(_AlbedoTex, IN.uv);

				//Calculate ambient color
				float3 Iamb = _Ambient * albedo * unity_AmbientSky; //Ambient term would be better to calculate per-vertex, because it's constant for all fragments.
				//Calculate diffuse color
				float3 Idiff = _LightColor0 * _Diffuse * albedo * max(0, dot(normal, lightDirection));
				//Calculate specular color
				float3 viewVector = normalize(_WorldSpaceCameraPos - IN.worldPos);
				float3 halfVector = normalize(viewVector + lightDirection);
				float3 Ispec = _LightColor0 * albedo * _Specular * pow(max(0, dot(halfVector, normal)), _SpecularExponent);

				return float4(Iamb + Idiff + Ispec, 1);
			}

			ENDCG
		}
	}
}
