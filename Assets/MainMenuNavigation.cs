﻿using System.Collections;
using System.Collections.Generic;
using Assets.Lib;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuNavigation : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        Level.First();
        Points.Reset();
    }

    public void StartGame()
    {
        SceneManager.LoadScene(Constants.SceneNameGame, LoadSceneMode.Single);
    }

    public void GoBackToMainMenu()
    {
        SceneManager.LoadScene(Constants.SceneNameMainMenu, LoadSceneMode.Single);
    }
}
