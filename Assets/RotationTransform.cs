﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationTransform : MonoBehaviour {

    public GameObject obj;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float yRotation = this.transform.eulerAngles.y;
        Debug.Log(yRotation);

        obj.transform.eulerAngles = new Vector3(obj.transform.eulerAngles.x, yRotation, obj.transform.eulerAngles.z);

        
	}
}
