﻿using System.Collections;
using System.Collections.Generic;
using Assets.Lib;
using UnityEngine;

public class CollisionController : MonoBehaviour
{
    public List<GameObject> CollisionTargets = new List<GameObject>();

    public GameObject Goat;

    private bool started = false;
    private bool dead = false;
    private bool won = false;

    void Start()
    {
        if (CollisionTargets == null)
        {
            CollisionTargets = new List<GameObject>();
        }

        Events.CountDownCompleted.Triggered += CountDownCompleted_Triggered;
        Events.GoatWin.Triggered += GoatWin_Triggered;

        Debug.Log("CollisionController Start");
    }

    void CountDownCompleted_Triggered(object sender, System.EventArgs e)
    {
        StartCollisionChecking();
        Events.CountDownCompleted.Triggered -= CountDownCompleted_Triggered;
    }

    void GoatWin_Triggered(object sender, System.EventArgs e)
    {
        won = true;
        Events.GoatWin.Triggered -= GoatWin_Triggered;
    }

    private void StartCollisionChecking()
    {
        started = true;
    }

    void Update()
    {
        if (started && !dead && !won)
        {
            Bounds goatBounds = GetBounds(Goat);

            for (int i = 0, length = CollisionTargets.Count; i < length; i++)
            {
                GameObject collisionTarget = CollisionTargets[i];
                Bounds targetBounds = GetBounds(collisionTarget);

                if (Overlapping3D(goatBounds, targetBounds))
                {
                    KillTheDamnGoat();
                    break;
                }
            }
        }
    }

    private bool Overlapping3D(Bounds goatBounds, Bounds targetBounds)
    {
        return     Overlapping(goatBounds.min.x, goatBounds.max.x,
                               targetBounds.min.x, targetBounds.max.x)

                && Overlapping(goatBounds.min.y, goatBounds.max.y,
                               targetBounds.min.y, targetBounds.max.y)

                && Overlapping(goatBounds.min.z, goatBounds.max.z,
                               targetBounds.min.z, targetBounds.max.z);
    }

    private Bounds GetBounds(GameObject gameObject)
    {
        BoxCollider boxCollider = gameObject.GetComponent<BoxCollider>();
        LODGroup lodGroup;

        if (boxCollider != null)
        {
            return boxCollider.bounds;
        }
        else if ((lodGroup = gameObject.GetComponentInChildren<LODGroup>()) != null)
        {
            LOD lod = lodGroup.GetLODs()[0];
            return lod.renderers[0].bounds;
        }
        else
        {
            return new Bounds(gameObject.transform.position, gameObject.transform.lossyScale);
        }
    }

    private bool Overlapping(float minA, float maxA, float minB, float maxB)
    {
        return minA < maxB && maxA > minB;
    }

    // AUTOMATIC COLLISION IS DISABLED
    //void OnCollisionEnter(Collision col)
    //{
    //    KillTheDamnGoat();
    //}

    private void KillTheDamnGoat()
    {
        NotifyGoatDied();
        Debug.Log("The damn goat diedededed!");
    }

    private void NotifyGoatDied()
    {
        if (!dead)
        {
            dead = true;
            Events.GoatDeath.Trigger();
        }
    }
}
