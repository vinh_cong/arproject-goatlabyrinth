﻿//using System;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Vuforia;
using OpenCVForUnity;

public class BorderInstantiation : MonoBehaviour, ITrackableEventHandler
{
    public GameObject[] Walls = new GameObject[4];

    public TrackableBehaviour ImageTarget1;
    public TrackableBehaviour ImageTarget2;
    //public GameObject ImageTarget1;
    //public GameObject ImageTarget2;
    public Material material;

    private static float x_low = -50;
    private static float x_high = 50;
    private static float z_low = -50;
    private static float z_high = 50;
    public static float height = 0;
    private float[] coordinates;
    private static float border_height = height + 10;

    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;
    private Mesh mesh;

    void Start()
    {
        meshFilter = gameObject.AddComponent<MeshFilter>();
        meshRenderer = gameObject.AddComponent<MeshRenderer>();
        mesh = meshFilter.mesh;

        mesh.vertices = new Vector3[]
        {
            new Vector3(x_low, height, z_low),
            new Vector3(x_low, border_height, z_low),
            new Vector3(x_low, height, z_high),
            new Vector3(x_low, border_height, z_high),
            new Vector3(x_high, height, z_high),
            new Vector3(x_high, border_height, z_high),
            new Vector3(x_high, height, z_low),
            new Vector3(x_high, border_height, z_low),
        };

        UpdateWallColliders(x_low, x_high,
            0, border_height,
            z_low, z_high);

        meshRenderer.materials = new Material[] { material };
        meshRenderer.material = material;
        DrawTriangles();
    }

    // Update is called once per frame
    void Update()
    {
        coordinates = BorderPoints(ImageTarget1, ImageTarget2);

        //Debug.Log("x_low: " + coordinates[0] + " + x_high: " + coordinates[1]);
        //Debug.Log("z_low: " + coordinates[2] + " + z_high: " + coordinates[3]);

        //Debug.Log("Position borderTarget1: x = " + ImageTarget1.transform.position.x + ", z = " + ImageTarget1.transform.position.z);
        //Debug.Log("Position borderTarget2: x = " + ImageTarget2.transform.position.x + ", z = " + ImageTarget2.transform.position.z);

        //Debug.Log("ImageTarget1: " + ImageTarget1.CurrentStatus);
        //Debug.Log("ImageTarget2: " + ImageTarget2.CurrentStatus);

        if (ImageTarget1.CurrentStatus.ToString().Equals("TRACKED") && ImageTarget2.CurrentStatus.ToString().Equals("TRACKED"))
        //{
        //VuMarkBehaviour VuMark1 = ImageTarget1.transform.parent.gameObject.GetComponent<VuMarkBehaviour>();
        //VuMarkBehaviour VuMark2 = ImageTarget2.transform.parent.gameObject.GetComponent<VuMarkBehaviour>();

        //if (ImageTarget1.GetComponent<Renderer>().enabled || ImageTarget2.GetComponent<Renderer>().enabled)

            //VuMark1.CurrentStatus.ToString().Equals("TRACKED") && VuMark2.CurrentStatus.ToString().Equals("TRACKED") &&
            //VuMark1.VuMarkTarget.InstanceId.StringValue.Equals("bor1") && VuMark2.VuMarkTarget.InstanceId.StringValue.Equals("bor2"))
        { 
            mesh.vertices = new Vector3[]
                {
            new Vector3(coordinates[0], 0F, coordinates[2]),
            new Vector3(coordinates[0], border_height, coordinates[2]),
            new Vector3(coordinates[0], 0F, coordinates[3]),
            new Vector3(coordinates[0], border_height, coordinates[3]),
            new Vector3(coordinates[1], 0F, coordinates[3]),
            new Vector3(coordinates[1], border_height, coordinates[3]),
            new Vector3(coordinates[1], 0F, coordinates[2]),
            new Vector3(coordinates[1], border_height, coordinates[2]),
                };

            UpdateWallColliders(coordinates[0], coordinates[1],
                0, border_height,
                coordinates[2], coordinates[3]);

            DrawTriangles();
        }

        //gameObject.transform.position = new Vector3(0, 0, 0);
        //Matrix4x4 Tra1 = T(-gameObject.transform.position.x, -gameObject.transform.position.y, -gameObject.transform.position.z);
        //gameObject.transform.position = Tra1.GetColumn(3);
        //}

    }

    float[] BorderPoints(TrackableBehaviour a, TrackableBehaviour b)
    //float[] BorderPoints(GameObject a, GameObject b)
    {
        float[] result = new float[4];
        float x_mid;
        float z_mid;

        if (a.transform.position.x < b.transform.position.x)
        {
            result[0] = a.transform.position.x;
            result[1] = b.transform.position.x;
        }
        else
        {
            result[0] = b.transform.position.x;
            result[1] = a.transform.position.x;
        }

        if (a.transform.position.z < b.transform.position.z)
        {
            result[2] = a.transform.position.z;
            result[3] = b.transform.position.z;
        }
        else
        {
            result[2] = b.transform.position.z;
            result[3] = a.transform.position.z;
        }

        // Center the arena in the world
        x_mid = (result[0] + result[1]) / 2;
        z_mid = (result[2] + result[3]) / 2;

        result[0] = result[0] - x_mid - 7.5f;
        result[1] = result[1] - x_mid + 7.5f;
        result[2] = result[2] - z_mid - 7.5f;
        result[3] = result[3] - z_mid + 7.5f;

        return result;
    }

    private void UpdateWallColliders(float xMin, float xMax, int yMin, float yMax, float zMin, float zMax)
    {
        BoxCollider boxCollider1 = BoxColliderForWallAtIndex(0);
        BoxCollider boxCollider2 = BoxColliderForWallAtIndex(1);
        BoxCollider boxCollider3 = BoxColliderForWallAtIndex(2);
        BoxCollider boxCollider4 = BoxColliderForWallAtIndex(3);

        UpdateBounds(boxCollider1, new Vector3(xMin - 2, yMin, zMin), new Vector3(xMin, yMax, zMax));
        UpdateBounds(boxCollider2, new Vector3(xMin, yMin, zMin - 2), new Vector3(xMax, yMax, zMin));
        UpdateBounds(boxCollider3, new Vector3(xMax, yMin, zMin), new Vector3(xMax + 2, yMax, zMax));
        UpdateBounds(boxCollider4, new Vector3(xMin, yMin, zMax), new Vector3(xMax, yMax, zMax + 2));
    }

    private BoxCollider BoxColliderForWallAtIndex(int index)
    {
        return Walls[index].GetComponent<BoxCollider>();
    }

    private void UpdateBounds(BoxCollider boxCollider, Vector3 min, Vector3 max)
    {
        Bounds bounds = new Bounds();
        bounds.SetMinMax(min, max);

        boxCollider.center = bounds.center;
        boxCollider.size = bounds.size;
    }

    void DrawTriangles()
    {
        int[] tri;
        tri = new int[48];

        // INSIDE
        tri[0] = 0;
        tri[1] = 1;
        tri[2] = 2;

        tri[3] = 1;
        tri[4] = 3;
        tri[5] = 2;

        tri[6] = 2;
        tri[7] = 3;
        tri[8] = 4;

        tri[9] = 3;
        tri[10] = 5;
        tri[11] = 4;

        tri[12] = 4;
        tri[13] = 5;
        tri[14] = 6;

        tri[15] = 5;
        tri[16] = 7;
        tri[17] = 6;

        tri[18] = 6;
        tri[19] = 7;
        tri[20] = 0;

        tri[21] = 7;
        tri[22] = 1;
        tri[23] = 0;

        // OUTSIDE
        tri[24] = 2;
        tri[25] = 1;
        tri[26] = 0;

        tri[27] = 2;
        tri[28] = 3;
        tri[29] = 1;

        tri[30] = 4;
        tri[31] = 3;
        tri[32] = 2;

        tri[33] = 4;
        tri[34] = 5;
        tri[35] = 3;

        tri[36] = 6;
        tri[37] = 5;
        tri[38] = 4;

        tri[39] = 6;
        tri[40] = 7;
        tri[41] = 5;

        tri[42] = 0;
        tri[43] = 7;
        tri[44] = 6;

        tri[45] = 0;
        tri[46] = 1;
        tri[47] = 7;

        mesh.triangles = tri;
    }

    void ITrackableEventHandler.OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        throw new NotImplementedException();
    }
}
