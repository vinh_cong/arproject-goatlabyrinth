﻿using UnityEngine;
using System.Collections;

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif

namespace OpenCVForUnitySample
{
	public class OpenCVForUnitySample : MonoBehaviour
	{

		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		public void OnShowLicenseButton ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("ShowLicense");
			#else
			Application.LoadLevel ("ShowLicense");
			#endif
		}

		public void OnTexture2DToMatSample ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("Texture2DToMatSample");
			#else
			Application.LoadLevel ("Texture2DToMatSample");
			#endif
		}

		public void OnThresholdSample ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("ThresholdSample");
			#else
			Application.LoadLevel ("ThresholdSample");
			#endif
		}

		public void OnDrawingSample ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("DrawingSample");
			#else
			Application.LoadLevel ("DrawingSample");
			#endif
		}

		public void OnConvexHullSample ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("ConvexHullSample");
			#else
			Application.LoadLevel ("ConvexHullSample");
			#endif
		}

		public void OnHoughLinesPSample ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("HoughLinesPSample");
			#else
			Application.LoadLevel ("HoughLinesPSample");
			#endif
		}

		public void OnFeature2DSample ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("Feature2DSample");
			#else
			Application.LoadLevel ("Feature2DSample");
			#endif
		}

		public void OnWrapPerspectiveSample ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("WrapPerspectiveSample");
			#else
			Application.LoadLevel ("WrapPerspectiveSample");
			#endif
		}

		public void OnDetectFaceSample ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("DetectFaceSample");
			#else
			Application.LoadLevel ("DetectFaceSample");
			#endif
		}

		public void OnWebCamTextureToMatSample ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("WebCamTextureToMatSample");
			#else
			Application.LoadLevel ("WebCamTextureToMatSample");
			#endif
		}

		public void OnWebCamTextureDetectFaceSample ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("WebCamTextureDetectFaceSample");
			#else
			Application.LoadLevel ("WebCamTextureDetectFaceSample");
			#endif
		}

		public void OnWebCamTextureAsyncDetectFaceSample ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("WebCamTextureAsyncDetectFaceSample");
			#else
			Application.LoadLevel ("WebCamTextureAsyncDetectFaceSample");
			#endif
		}

		public void OnComicFilterSample ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("ComicFilterSample");
			#else
			Application.LoadLevel ("ComicFilterSample");
			#endif
		}

		public void OnCamShiftSample ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("CamShiftSample");
			#else
			Application.LoadLevel ("CamShiftSample");
			#endif
		}

		public void OnHandPoseEstimationSample ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("HandPoseEstimationSample");
			#else
			Application.LoadLevel ("HandPoseEstimationSample");
			#endif
		}
	}
}
		