﻿using System.Collections;
using System.Collections.Generic;
using Assets.Lib;
using UnityEngine;

public class DirectionController : MonoBehaviour
{
    public GameObject Goat;
    public GameObject ImageTarget;
    public GameObject GoatModel;
    
    private float MovementSpeed;

    float xvelocity;
    float zvelocity;
    float xdistance;
    float zdistance;
    float yRotation;
    float yRotationinDegrees;

    private bool ended = false;
    private bool started = false;

    // Use this for initialization
    void Start()
    {
        Events.GoatDeath.Triggered += GoatDeath_Triggered;
        Events.CountDownCompleted.Triggered += CountDownCompleted_Triggered;
    }

    void GoatDeath_Triggered(object sender, System.EventArgs e)
    {
        StopMoving();
        Events.GoatDeath.Triggered -= GoatDeath_Triggered;
    }

    void CountDownCompleted_Triggered(object sender, System.EventArgs e)
    {
        StartMoving();
        Events.CountDownCompleted.Triggered -= CountDownCompleted_Triggered;
    }

    private void StopMoving()
    {
        ended = true;
        //enabled = false;
    }

    private void StartMoving()
    {
        MovementSpeed = GoatSpeed.Current;
        started = true;
        Animation animator = GoatModel.GetComponent<Animation>();
        animator.CrossFade("walkGot");
        animator["walkGot"].speed = MovementSpeed / GoatSpeed.Base;
    }

    // Update is called once per frame
    void Update()
    {
        if (ended || !started)
        {
            return;
        }

        //Facing of the goat
        yRotation = ImageTarget.transform.eulerAngles.y;
        Goat.transform.eulerAngles = 
            new Vector3(Goat.transform.eulerAngles.x, yRotation, Goat.transform.eulerAngles.z);

        //Prior positioning in regards to world border markers
        Matrix4x4 StartTra = T(0, 0, 0);

        //Movement in x- and z-axises
        yRotationinDegrees = yRotation * Mathf.Deg2Rad;
        zvelocity = Mathf.Sin((yRotationinDegrees));
        xvelocity = Mathf.Cos((yRotationinDegrees));

        xdistance = xdistance + xvelocity * Time.deltaTime * MovementSpeed;
        zdistance = zdistance + zvelocity * Time.deltaTime * MovementSpeed;

        Matrix4x4 Tra = T(xdistance, 0, -zdistance);

        Matrix4x4 GoatTransformation = Tra * StartTra;

        //Applying transformations to obj
        Goat.transform.position = GoatTransformation.GetColumn(3);
    }

    // Scale
    Matrix4x4 S(float sx, float sy, float sz)
    {
        Matrix4x4 m = new Matrix4x4();

        m.SetRow(0, new Vector4(sx, 0, 0, 0));
        m.SetRow(1, new Vector4(0, sy, 0, 0));
        m.SetRow(2, new Vector4(0, 0, sz, 0));
        m.SetRow(3, new Vector4(0, 0, 0, 1));

        return m;
    }

    // Translate
    Matrix4x4 T(float tx, float ty, float tz)
    {
        Matrix4x4 m = new Matrix4x4();

        m.SetRow(0, new Vector4(1, 0, 0, tx));
        m.SetRow(1, new Vector4(0, 1, 0, ty));
        m.SetRow(2, new Vector4(0, 0, 1, tz));
        m.SetRow(3, new Vector4(0, 0, 0, 1));

        return m;
    }

    // Rotate around y-axis
    Matrix4x4 Ry(float angle)
    {
        Matrix4x4 m = new Matrix4x4();

        m.SetRow(0, new Vector4(Mathf.Cos(angle), 0, -Mathf.Sin(angle), 0));
        m.SetRow(1, new Vector4(0, 1, 0, 0));
        m.SetRow(2, new Vector4(Mathf.Sin(angle), 0, Mathf.Cos(angle), 0));
        m.SetRow(3, new Vector4(0, 0, 0, 1));

        return m;
    }

    // Rotate around x-axis
    Matrix4x4 Rx(float angle)
    {
        Matrix4x4 m = new Matrix4x4();

        m.SetRow(0, new Vector4(1, 0, 0, 0));
        m.SetRow(1, new Vector4(0, Mathf.Cos(angle), -Mathf.Sin(angle), 0));
        m.SetRow(2, new Vector4(0, Mathf.Sin(angle), Mathf.Cos(angle), 0));
        m.SetRow(3, new Vector4(0, 0, 0, 1));

        return m;
    }
    Matrix4x4 Rz(float angle)
    {
        Matrix4x4 m = new Matrix4x4();

        m.SetRow(0, new Vector4(Mathf.Cos(angle), -Mathf.Sin(angle), 0, 0));
        m.SetRow(1, new Vector4(Mathf.Sin(angle), Mathf.Cos(angle), 0, 0));
        m.SetRow(2, new Vector4(0, 0, 1, 0));
        m.SetRow(3, new Vector4(0, 0, 0, 1));

        return m;
    }
}
