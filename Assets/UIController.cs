﻿using System.Collections;
using System.Collections.Generic;
using Assets.Lib;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Text txfPoints;
    public Text txfCountDown;
    public Text txfAddedPoints;
    public Text txfLevel;

    // Use this for initialization
    void Start()
    {
        Events.CountDownAdvanced.Triggered += CountDownAdvanced_Triggered;
        Events.GoatDeath.Triggered += GoatDeath_Triggered;
        Events.GoatWin.Triggered += GoatWin_Triggered;
        Events.LevelChanged.Triggered += LevelChanged_Triggered;
        Events.PointsEarned.Triggered += PointsEarned_Triggered;
        SetCurrentLevel();
        SetCurrentPoints();
    }

    void CountDownAdvanced_Triggered(object sender, System.EventArgs e)
    {
        ShowCurrentCountDownNumber();
    }

    void GoatDeath_Triggered(object sender, System.EventArgs e)
    {
        Unsubscribe();
    }

    void GoatWin_Triggered(object sender, System.EventArgs e)
    {
        Unsubscribe();
    }

    private void Unsubscribe()
    {
        Events.CountDownAdvanced.Triggered -= CountDownAdvanced_Triggered;
        Events.GoatDeath.Triggered -= GoatDeath_Triggered;
        Events.GoatWin.Triggered -= GoatWin_Triggered;
        Events.LevelChanged.Triggered -= LevelChanged_Triggered;
        Events.PointsEarned.Triggered -= PointsEarned_Triggered;
    }

    void LevelChanged_Triggered(object sender, System.EventArgs e)
    {
        SetCurrentLevel();
    }

    void PointsEarned_Triggered(object sender, PointsEventArgs e)
    {
        UpdatePoints(e.Points);
    }

    private void UpdatePoints(int points)
    {
        SetCurrentPoints();
        IndicatePointsWereAdded(points);
    }

    private void SetCurrentPoints()
    {
        txfPoints.text = string.Format("Points: {0}", Points.Current);
    }

    private void SetCurrentLevel()
    {
        txfLevel.text = string.Format("Level: {0}", Level.Current);
    }

    private void ShowCurrentCountDownNumber()
    {
        txfCountDown.text = CountDown.Current.ToString();
        Animator countDownAnimation = txfCountDown.GetComponent<Animator>();
        countDownAnimation.Play("CountdownNumber");
    }

    private void IndicatePointsWereAdded(int points)
    {
        txfAddedPoints.text = string.Format("+{0}", points);
        Animator addedAnimation = txfAddedPoints.GetComponent<Animator>();
        addedAnimation.Play("PointsAdded");
    }
}
