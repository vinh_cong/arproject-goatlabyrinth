﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {

    // Scale
    Matrix4x4 S(float sx, float sy, float sz)
    {
        Matrix4x4 m = new Matrix4x4();

        m.SetRow(0, new Vector4(sx, 0, 0, 0));
        m.SetRow(1, new Vector4(0, sy, 0, 0));
        m.SetRow(2, new Vector4(0, 0, sz, 0));
        m.SetRow(3, new Vector4(0, 0, 0, 1));

        return m;
    }

    // Translate
    Matrix4x4 T(float tx, float ty, float tz)
    {
        Matrix4x4 m = new Matrix4x4();

        m.SetRow(0, new Vector4(1, 0, 0, tx));
        m.SetRow(1, new Vector4(0, 1, 0, ty));
        m.SetRow(2, new Vector4(0, 0, 1, tz));
        m.SetRow(3, new Vector4(0, 0, 0, 1));

        return m;
    }

    // Rotate around y-axis
    Matrix4x4 Ry(float angle)
    {
        Matrix4x4 m = new Matrix4x4();

        m.SetRow(0, new Vector4(Mathf.Cos(angle), 0, -Mathf.Sin(angle), 0));
        m.SetRow(1, new Vector4(0, 1, 0, 0));
        m.SetRow(2, new Vector4(Mathf.Sin(angle), 0, Mathf.Cos(angle), 0));
        m.SetRow(3, new Vector4(0, 0, 0, 1));

        return m;
    }

    // Rotate around x-axis
    Matrix4x4 Rx(float angle)
    {
        Matrix4x4 m = new Matrix4x4();

        m.SetRow(0, new Vector4(1, 0, 0, 0));
        m.SetRow(1, new Vector4(0, Mathf.Cos(angle), -Mathf.Sin(angle), 0));
        m.SetRow(2, new Vector4(0, Mathf.Sin(angle), Mathf.Cos(angle), 0));
        m.SetRow(3, new Vector4(0, 0, 0, 1));

        return m;
    }
    Matrix4x4 Rz(float angle)
    {
        Matrix4x4 m = new Matrix4x4();

        m.SetRow(0, new Vector4(Mathf.Cos(angle), -Mathf.Sin(angle), 0, 0));
        m.SetRow(1, new Vector4(Mathf.Sin(angle), Mathf.Cos(angle), 0, 0));
        m.SetRow(2, new Vector4(0, 0, 1, 0));
        m.SetRow(3, new Vector4(0, 0, 0, 1));

        return m;
    }

}
