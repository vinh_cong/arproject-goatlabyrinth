﻿using System.Collections;
using System.Collections.Generic;
using Assets.Lib;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinSequenceController : MonoBehaviour
{

    public GameObject GoatModel;

    // Use this for initialization
    void Start()
    {
        Animation animator = GoatModel.GetComponent<Animation>();
        float animationLength = animator["walkGot"].length;
        StopGoatWalkAndMakeItIdleAfter(animationLength);
    }

    private void StopGoatWalkAndMakeItIdleAfter(float duration)
    {
        StartCoroutine(SwitchGoatAnimationAfter(duration));
    }

    private IEnumerator SwitchGoatAnimationAfter(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Animation animator = GoatModel.GetComponent<Animation>();
        animator.CrossFade("idleGot");
        yield return new WaitForSeconds(0);
    }
}
