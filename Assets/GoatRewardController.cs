﻿using System.Collections;
using System.Collections.Generic;
using Assets.Lib;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoatRewardController : MonoBehaviour
{
    public int PrizePoints = 5;
    public int WinPoints = 15;

    public GameObject Goat;
    public GameObject WinTarget;

    public GameObject BoundingTarget1;
    public GameObject BoundingTarget2;

    public GameObject Prize;

    private bool started = false;
    private bool dead = false;
    private bool won = false;

    void Start()
    {
        Events.CountDownCompleted.Triggered += CountDownCompleted_Triggered;
        Events.GoatDeath.Triggered += GoatDeath_Triggered;

        Debug.Log("GoatRewardController Start");
    }

    void CountDownCompleted_Triggered(object sender, System.EventArgs e)
    {
        StartRewarding();
        Events.CountDownCompleted.Triggered -= CountDownCompleted_Triggered;
    }

    void GoatDeath_Triggered(object sender, System.EventArgs e)
    {
        dead = true;
        Events.GoatDeath.Triggered -= GoatDeath_Triggered;
    }

    private void StartRewarding()
    {
        started = true;
        SpawnPrize();
    }

    private void SpawnPrize()
    {
        Vector3 pos1 = BoundingTarget1.transform.position;
        Vector3 pos2 = BoundingTarget2.transform.position;

        Vector3 min = new Vector3(Mathf.Min(pos1.x, pos2.x),
            Mathf.Min(pos1.y, pos2.y),
            Mathf.Min(pos1.z, pos2.z));
        Vector3 max = new Vector3(Mathf.Max(pos1.x, pos2.x),
            Mathf.Max(pos1.y, pos2.y),
            Mathf.Max(pos1.z, pos2.z));

        Vector3 randomPrizePosition = new Vector3(Random.Range(min.x, max.x), 
            Goat.transform.lossyScale.y / 2, 
            Random.Range(min.z, max.z));

        Prize.transform.position = randomPrizePosition;
    }

    void Update()
    {
        if (started && !dead && !won)
        {
            Bounds goatBounds = GetBounds(Goat);

            Bounds prizeBounds = GetBounds(Prize);
            Bounds winBounds = GetBounds(WinTarget);


            if (Overlapping3D(goatBounds, prizeBounds))
            {
                GivePrize();
                SpawnPrize();
            }
            if (Overlapping3D(goatBounds, winBounds))
            {
                ThisIsAWin();
            }
        }
    }

    private void GivePrize()
    {
        Points.Add(PrizePoints);
    }

    private void ThisIsAWin()
    {
        Points.Add(WinPoints);
        Level.Next();
        Events.GoatWin.Trigger();
    }

    private bool Overlapping3D(Bounds goatBounds, Bounds targetBounds)
    {
        return     Overlapping(goatBounds.min.x, goatBounds.max.x,
                               targetBounds.min.x, targetBounds.max.x)

                && Overlapping(goatBounds.min.y, goatBounds.max.y,
                               targetBounds.min.y, targetBounds.max.y)

                && Overlapping(goatBounds.min.z, goatBounds.max.z,
                               targetBounds.min.z, targetBounds.max.z);
    }

    private Bounds GetBounds(GameObject gameObject)
    {
        BoxCollider boxCollider = gameObject.GetComponent<BoxCollider>();
        LODGroup lodGroup;

        if (boxCollider != null)
        {
            return boxCollider.bounds;
        }
        else if ((lodGroup = gameObject.GetComponentInChildren<LODGroup>()) != null)
        {
            LOD lod = lodGroup.GetLODs()[0];
            return lod.renderers[0].bounds;
        }
        else
        {
            return new Bounds(gameObject.transform.position, gameObject.transform.lossyScale);
        }
    }

    private bool Overlapping(float minA, float maxA, float minB, float maxB)
    {
        return minA < maxB && maxA > minB;
    }

    // AUTOMATIC COLLISION IS DISABLED
    //void OnCollisionEnter(Collision col)
    //{
    //    ThisIsAWin();
    //}
}
