﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Lib
{
    public static class Points
    {
        private static int current = 0;

        public static void Reset()
        {
            current = 0;
        }

        public static void Add(int points)
        {
            current += points;
            Events.PointsEarned.Trigger(points);
        }

        public static int Current
        {
            get
            {
                return current;
            }
        }
    }
}
