﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Lib
{
    public static class Level
    {
        private static int current = 1;

        public static void First()
        {
            Debug.Log("First Level");
            current = 1;
            Events.LevelChanged.Trigger();
        }

        public static void Next()
        {
            current += 1;
            Debug.Log("Next Level: " + current);
            Events.LevelChanged.Trigger();
        }

        public static int Current
        {
            get
            {
                return current;
            }
        }
    }
}
