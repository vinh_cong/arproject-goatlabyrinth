﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Lib
{
    public static class Events
    {
        private static Event goatDeath = new Event();
        private static Event goatWin = new Event();
        private static PointsEvent pointsEarned = new PointsEvent();
        private static Event levelChanged = new Event();
        private static Event countDownAdvanced = new Event();
        private static Event countDownCompleted = new Event();

        public static Event GoatDeath
        {
            get
            {
                return goatDeath;
            }
        }

        public static Event GoatWin
        {
            get
            {
                return goatWin;
            }
        }

        public static PointsEvent PointsEarned
        {
            get
            {
                return pointsEarned;
            }
        }

        public static Event LevelChanged
        {
            get
            {
                return levelChanged;
            }
        }

        public static Event CountDownAdvanced
        {
            get
            {
                return countDownAdvanced;
            }
        }

        public static Event CountDownCompleted
        {
            get
            {
                return countDownCompleted;
            }
        }
    }
}
