﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Lib
{
    public static class Constants
    {
        public static readonly string SceneNameMainMenu = "MainMenu";
        public static readonly string SceneNameGame = "MyScene";
        public static readonly string SceneNameWin = "GoatWin";
        public static readonly string SceneNameDeath = "GoatDeath";
    }
}
