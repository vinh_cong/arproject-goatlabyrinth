﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Lib
{
    public class Event
    {
        public event EventHandler Triggered;

        public void Trigger()
        {
            OnTriggered();
        }

        protected virtual void OnTriggered()
        {
            if (Triggered != null)
            {
                Triggered(this, EventArgs.Empty);
            }
        }
    }
}
