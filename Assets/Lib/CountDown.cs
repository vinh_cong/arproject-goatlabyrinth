﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Lib
{
    public class CountDown
    {
        private static int current = 0;

        public static void Start(int startValue)
        {
            current = startValue;
            Advance();
        }

        public static void Next()
        {
            if (IsActive)
            {
                current -= 1;
                Advance();
            }
        }

        private static void Advance()
        {
            Events.CountDownAdvanced.Trigger();

            if (!IsActive)
            {
                Events.CountDownCompleted.Trigger();
            }
        }

        public static int Current
        {
            get
            {
                return current;
            }
        }

        public static bool IsActive
        {
            get
            {
                return current > 0;
            }
        }
    }
}
