﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Lib
{
    public class PointsEventArgs : EventArgs
    {
        public int Points { get; protected set; }

        public PointsEventArgs(int points)
        {
            this.Points = points;
        }
    }
}
