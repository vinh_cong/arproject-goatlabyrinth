﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Lib
{
    public static class GoatSpeed
    {
        private static readonly int baseSpeed = 10;
        private static readonly int slope = 2;

        public static int Current
        {
            get
            {
                return baseSpeed + slope * Level.Current;
            }
        }

        public static int Base
        {
            get
            {
                return baseSpeed + slope;
            }
        }
    }
}
