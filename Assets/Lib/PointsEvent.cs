﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Lib
{
    public delegate void PointsEventHandler(object sender, PointsEventArgs e);

    public class PointsEvent
    {
        public event PointsEventHandler Triggered;

        public void Trigger(int points)
        {
            OnTriggered(points);
        }

        protected virtual void OnTriggered(int points)
        {
            if (Triggered != null)
            {
                Triggered(this, new PointsEventArgs(points));
            }
        }
    }
}
