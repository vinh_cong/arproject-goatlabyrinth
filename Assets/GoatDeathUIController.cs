﻿using System.Collections;
using System.Collections.Generic;
using Assets.Lib;
using UnityEngine;
using UnityEngine.UI;

public class GoatDeathUIController : MonoBehaviour
{
    public Text txfPoints;
    public Text txfLevel;

    // Use this for initialization
    void Start()
    {
        SetCurrentLevel();
        SetCurrentPoints();
    }

    private void SetCurrentPoints()
    {
        if (txfPoints != null)
        {
            txfPoints.text = string.Format("Points: {0}", Points.Current);
        }
    }

    private void SetCurrentLevel()
    {
        if (txfLevel != null)
        {
            txfLevel.text = string.Format("Level: {0}", Level.Current);
        }
    }
}

