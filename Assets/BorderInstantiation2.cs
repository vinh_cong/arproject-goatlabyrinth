﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Vuforia;

public class BorderInstantiation2 : MonoBehaviour
{
    //public TrackableBehaviour ImageTarget1;
    //public TrackableBehaviour ImageTarget2;
    public GameObject ImageTarget1;
    public GameObject ImageTarget2;
    public Material material;

    //public GameObject cube;

    private static float x_low = -50;
    private static float x_high = 50;
    private static float z_low = -50;
    private static float z_high = 50;
    private float[] coordinates;
    private static float border_height = 100;

    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;
    private Mesh mesh;

    void Start()
    {
        meshFilter = gameObject.AddComponent<MeshFilter>();
        //meshFilter = GetComponent<MeshFilter>();
        meshRenderer = gameObject.AddComponent<MeshRenderer>();
        //meshRenderer = GetComponent<MeshRenderer>();

        mesh = meshFilter.mesh;

        mesh.vertices = new Vector3[]
        {
            new Vector3(x_low, 0F, z_low),
            new Vector3(x_low, border_height, z_low),
            new Vector3(x_low, 0F, z_high),
            new Vector3(x_low, border_height, z_high),
            new Vector3(x_high, 0F, z_high),
            new Vector3(x_high, border_height, z_high),
            new Vector3(x_high, 0F, z_low),
            new Vector3(x_high, border_height, z_low),
        };

        meshRenderer.materials = new Material[] { material };
        meshRenderer.material = material;
        DrawTriangles();
    }



    // Update is called once per frame
    void Update()
    {
        coordinates = BorderPoints(ImageTarget1, ImageTarget2);

        //Debug.Log("ImageTarget1: " + ImageTarget1.CurrentStatus);
        //Debug.Log("ImageTarget2: " + ImageTarget2.CurrentStatus);

        //if (ImageTarget1.CurrentStatus.ToString().Equals("TRACKED") && ImageTarget2.CurrentStatus.ToString().Equals("TRACKED"))
        //{
        mesh.vertices = new Vector3[]
            {
            new Vector3(coordinates[0], 0F, coordinates[2]),
            new Vector3(coordinates[0], border_height, coordinates[2]),
            new Vector3(coordinates[0], 0F, coordinates[3]),
            new Vector3(coordinates[0], border_height, coordinates[3]),
            new Vector3(coordinates[1], 0F, coordinates[3]),
            new Vector3(coordinates[1], border_height, coordinates[3]),
            new Vector3(coordinates[1], 0F, coordinates[2]),
            new Vector3(coordinates[1], border_height, coordinates[2]),
            };
        // Debug.Log(ImageTarget1.name + " + " + ImageTarget2.name);
        DrawTriangles();

        //Instantiate(cube, new Vector3((coordinates[0]+coordinates[1]/2), 0, (coordinates[2] + coordinates[3] / 2)), Quaternion.identity);

        //gameObject.transform.position = new Vector3(0, 0, 0);
        //Matrix4x4 Tra1 = T(-gameObject.transform.position.x, -gameObject.transform.position.y, -gameObject.transform.position.z);
        //gameObject.transform.position = Tra1.GetColumn(3);
        //}

    }

    float[] BorderPoints(GameObject a, GameObject b)
    {
        float[] result = new float[4];

        if (a.transform.position.x < b.transform.position.x)
        {
            result[0] = a.transform.position.x + 2;
            result[1] = b.transform.position.x + 2;
        }
        else
        {
            result[0] = b.transform.position.x + 2;
            result[1] = a.transform.position.x + 2;
        }

        if (a.transform.position.z < b.transform.position.z)
        {
            result[2] = a.transform.position.z + 2;
            result[3] = b.transform.position.z + 2;
        }
        else
        {
            result[2] = b.transform.position.z + 2;
            result[3] = a.transform.position.z + 2;
        }

        return result;
    }

    void DrawTriangles()
    {
        int[] tri;
        tri = new int[48];

        // INSIDE
        tri[0] = 0;
        tri[1] = 1;
        tri[2] = 2;

        tri[3] = 1;
        tri[4] = 3;
        tri[5] = 2;

        tri[6] = 2;
        tri[7] = 3;
        tri[8] = 4;

        tri[9] = 3;
        tri[10] = 5;
        tri[11] = 4;

        tri[12] = 4;
        tri[13] = 5;
        tri[14] = 6;

        tri[15] = 5;
        tri[16] = 7;
        tri[17] = 6;

        tri[18] = 6;
        tri[19] = 7;
        tri[20] = 0;

        tri[21] = 7;
        tri[22] = 1;
        tri[23] = 0;

        // OUTSIDE
        tri[24] = 2;
        tri[25] = 1;
        tri[26] = 0;

        tri[27] = 2;
        tri[28] = 3;
        tri[29] = 1;

        tri[30] = 4;
        tri[31] = 3;
        tri[32] = 2;

        tri[33] = 4;
        tri[34] = 5;
        tri[35] = 3;

        tri[36] = 6;
        tri[37] = 5;
        tri[38] = 4;

        tri[39] = 6;
        tri[40] = 7;
        tri[41] = 5;

        tri[42] = 0;
        tri[43] = 7;
        tri[44] = 6;

        tri[45] = 0;
        tri[46] = 1;
        tri[47] = 7;

        mesh.triangles = tri;
    }
}
