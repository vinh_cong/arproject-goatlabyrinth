﻿using System.Collections;
using System.Collections.Generic;
using Assets.Lib;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    public AudioSource GoatSpeek;
    
    public AudioClip GoatDie;

    private bool ended = false;

    void Start()
    {
        Events.GoatDeath.Triggered += GoatDeath_Triggered;
        Events.GoatWin.Triggered += GoatWin_Triggered;
    }

    void GoatDeath_Triggered(object sender, System.EventArgs e)
    {
        HandleGoatDeath();
        Detach();
    }

    void GoatWin_Triggered(object sender, System.EventArgs e)
    {
        Detach();
    }

    private void Detach()
    {
        ended = true;
        Events.GoatDeath.Triggered -= GoatDeath_Triggered;
        Events.GoatWin.Triggered -= GoatWin_Triggered;
    }

    private void HandleGoatDeath()
    {
        ScreamInAgony();
    }

    private void ScreamInAgony()
    {
        if (!ended)
        {
            GoatSpeek.clip = GoatDie;
            GoatSpeek.Play();
        }
    }
}
