﻿using System.Collections;
using System.Collections.Generic;
using Assets.Lib;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public GameObject Goat;
    public GameObject GoatSteeringWheel;
    public GameObject GoatModel;

    void Start()
    {
        Events.GoatDeath.Triggered += GoatDeath_Triggered;
        Events.GoatWin.Triggered += GoatWin_Triggered;
        StartCountDown(30);
    }

    void GoatDeath_Triggered(object sender, System.EventArgs e)
    {
        HandleGoatDeath();
        Unsubscribe();
    }

    void GoatWin_Triggered(object sender, System.EventArgs e)
    {
        HandleGoatWin();
        Unsubscribe();
    }

    private void Unsubscribe()
    {
        Events.GoatDeath.Triggered -= GoatDeath_Triggered;
        Events.GoatWin.Triggered -= GoatWin_Triggered;
    }

    private void HandleGoatDeath()
    {
        KillGoat();
        Debug.Log("You STUPID GOAT!");
    }

    private void HandleGoatWin()
    {
        Debug.Log("You FEKN MAED IIT!!!");
        SceneManager.LoadScene(Constants.SceneNameWin, LoadSceneMode.Single);
    }

    private void KillGoat()
    {
        Animation animator = GoatModel.GetComponent<Animation>();
        animator.CrossFade("deathGot");
        float animationLength = animator["deathGot"].length;
        WhenAnimationIsDoneSwitchToGoatDeathScene(animationLength);
    }

    private void WhenAnimationIsDoneSwitchToGoatDeathScene(float animationLength)
    {
        StartCoroutine(WaitAndSwitchToScene(animationLength, Constants.SceneNameDeath));
    }

    private IEnumerator WaitAndSwitchToScene(float waitTime, string sceneName)
    {
        yield return new WaitForSeconds(waitTime);
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        yield return new WaitForSeconds(0);
    }

    private void StartCountDown(int startValue)
    {
        StartCoroutine(DoCountDown(startValue));
    }

    private IEnumerator DoCountDown(int startValue)
    {
        yield return new WaitForSeconds(1);
        CountDown.Start(startValue);

        while (CountDown.IsActive)
        {
            yield return new WaitForSeconds(1);
            CountDown.Next();
        }
    }
}
